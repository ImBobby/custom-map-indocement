;(function ( window, document, $, undefined ) {
    'use strict';

    var App = {

        init: function () {
            this.loadMapStyles();
        },

        loadMapStyles: function () {
            /**
             * Load multiple ajax file with 1 callback
             * http://davidwalsh.name/jquery-ajax-callback
             */
            $.when(
                $.getJSON('data/map.styles.json'),
                $.getJSON('data/map.markers.json'),
                $.getJSON('data/connected.markers.json'),
                $.getJSON('data/autocomplete.list.json')
            ).then( App.generateMap );
        },

        /**
         * @param a - ajax result for map.styles.json
         * @param b - ajax result for map.markers.json
         * @param c - ajax result for connected.markers.json
         * @param c - ajax result for autocomplete.list.json
         */
        generateMap: function ( a, b, c, d ) {
            // Get current windowo height
            var winHeight = window.innerHeight;

            // Check if screen size above 768px
            var isLarge = window.matchMedia('screen and (min-width: 48em)').matches;

            /**
             * So this equation is got from:
             * if winHeight = 600, the positionMultiplier = 2
             * if winHeight = 800, the positionMultiplier = 2.2
             *
             * using linear equation 
             * y - y1 = m(x - x1)
             * where 
             * m = (y2 - y1) / (x2 - x1)
             *
             * we got this equation
             * y = (x + 1400) / 1000
             *
             * where x = window.innerHeight;
             */
            var positionMultiplier  = (winHeight + 1400) / 1000;

            var mapStyles           = a[0].mapStyles;
            var markersData         = b[0].markers;
            var connectedMarkers    = c[0].markers;
            var autocompleteList    = d[0].list;

            var mapContainer = document.querySelector('#map');
            var mapOpts = {
                zoom: !isLarge ? 6 : 7,
                disableDoubleClickZoom: true,
                center: { lat: -5.553798, lng: 114.960938 },
                disableDefaultUI: true,
                scrollwheel: false,
                styles: mapStyles
            };

            // Draw the map
            var map = new google.maps.Map( mapContainer, mapOpts );

            // Create infoWindow
            var infoWindow = new google.maps.InfoWindow({
                zIndex: 10
            });

            // Create markers. return an array of markers.
            var markers = App.drawMarkers( mapContainer, markersData, map );

            // Setup autocomplete form
            App.setupFormLocation( autocompleteList, markers );

            // Draw curved lines between markers
            for (var i = 0; i < connectedMarkers.length; i++) {
                App.drawLine( connectedMarkers[i], map, mapContainer );
            }

            // Listen to marker clicked, then open infoWindow
            $(mapContainer).on('markerIsClicked', function(e, marker, markerEvent) {
                /**
                 * If a marker is clicked, we don't want the marker positioned
                 * to center. Instead we want the infoWindow to be center
                 * positioned. That is why, map is panned to
                 * latitude + positionMultiplier
                 */
                var pos = marker.getPosition();
                map.panTo({
                    lat: pos.k + positionMultiplier,
                    lng: pos.D
                });

                infoWindow.setContent( marker.dataFactory );
                infoWindow.open( map, marker );
            });
        },

        /**
         * @param {DOM} mapContainer
         * @param {Array of object} markersData
         * @param {google.maps.Map} map
         */
        drawMarkers: function ( mapContainer, markersData, map ) {
            var markers = [];

            // Register custom event named 'markerIsClicked'
            var markerListener = function (e) {
                $(mapContainer).trigger('markerIsClicked', [this, event]);
            };

            for (var i = 0; i < markersData.length; i++) {
                var currMarker  = markersData[i];
                var markerLat   = currMarker.position.lat;
                var markerLng   = currMarker.position.lng;
                var markerTitle = currMarker.name;
                var iconOpts;

                if ( currMarker.isMain ) {
                    iconOpts = App.getIconOpts(true);
                } else {
                    iconOpts = App.getIconOpts(false);
                }

                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(markerLat, markerLng),
                    title: markerTitle,
                    map: map,
                    icon: iconOpts
                });
                marker.dataFactory = currMarker.dataFactory;
                marker.dataFactoryName = currMarker.name;

                google.maps.event.addListener(marker, 'click', markerListener);

                markers.push( marker );
            }

            return markers;
        },

        /**
         * Draw curved line on top map
         * curved_lines.overfx.net/
         *
         * @param {object} markersData
         * @param {google.maps.Map} map
         * @param {DOM} mapContainer
         */
        drawLine: function ( markersData, map, mapContainer ) {
            var x1 = markersData.start[0];
            var y1 = markersData.start[1];
            var x2 = markersData.end[0];
            var y2 = markersData.end[1];

            // Set 'curviness' of line. The higher, the curvier.
            var multiplier = markersData.isHorizontal ? 2 : -2;

            $(mapContainer).curvedLine({
                LatStart: x1,
                LngStart: y1,
                LatEnd: x2,
                LngEnd: y2,
                Map: map,
                Color: '#FFFFFF',
                Weight: 1,
                GapWidth: 1,
                Opacity: 0.4,
                Resolution: 0.02,
                Multiplier: multiplier,
                Horizontal: markersData.isHorizontal
            });
        },

        /**
         * @param {boolean} isMain
         *
         * There are 2 types of marker, main and secondary. Each marker has
         * different size so we should adjust anchor and scaledSize accordingly.
         */
        getIconOpts: function ( isMain ) {
            if ( isMain ) {
                return {
                    url: 'img/marker-main.png',
                    anchor: new google.maps.Point(20, 20),
                    scaledSize: new google.maps.Size(40, 40)
                };
            }

            return {
                url: 'img/marker-secondary.png',
                anchor: new google.maps.Point(12, 12),
                scaledSize: new google.maps.Size(25, 25)
            };
        },

        setupFormLocation: function ( autocompleteList, markers ) {
            var $formLocation        = $('.form-map-location');
            var $formLocationInput   = $('.form-input');

            new Awesomplete($formLocationInput[0], {
                list: autocompleteList,
                minChars: 1
            });

            $formLocationInput.on('awesomplete-selectcomplete', function () {
                $formLocation.trigger('submit');
            });

            $formLocation.on('submit', function(event) {
                event.preventDefault();
                var selectedLocation = $formLocationInput.val();
                
                for (var i = markers.length - 1; i >= 0; i--) {
                    if ( markers[i].dataFactoryName === selectedLocation ) {
                        new google.maps.event.trigger( markers[i], 'click' );
                    }
                }
            });
        }

    };

    App.init();

})( window, document, jQuery );